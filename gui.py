#------------------------------------------------------------------
#-- UTEC - Universidad de Ingeniería y Tecnología
#-- Curso: Introducción a la Ciencia de la Computación
#-- Grupo: Lab. 1.10
#-- Proyecto: Corrector Ortográfico
#------------------------------------------------------------------

#------------------------------------------------------------------
#-- Interfaz Gráfica del Corrector Ortográfico
#-- Utilizando el tkinter
#------------------------------------------------------------------
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
from funciones import *

#------------------------------------------------------------------
#-- Definición de la Ventana Principal
#------------------------------------------------------------------
window = Tk()
window.title( "UTEC - ICC - Corrector Ortográfico" )
window.resizable(0, 0)
#window.geometry("600x400")

#------------------------------------------------------------------
#-- Definición de las Variables Globales
#------------------------------------------------------------------
messageVar = StringVar()
messageVar.set( "Bienvenido al Corrector Ortográfico ... !" )

# -- Texto a Corregir
textToCorrect = StringVar();
textToCorrect.set( "Hijo de gat caza ratóncito" )

# -- Palabra erronea
wordError = StringVar()
wordError.set( "gatonudo" )

# -- Lista de posible palabra correcta
listWordsCorrect = ["gato", "gata", "gas", "gam"]

# -- Lista de palabras erroneas
listWordsError = ["gat", "bubuja", "bogado"]

# -- Total de palabras mal escritas
totalWordsError = 0

# -- Pivot de las palabras mal escritas
pivotWordsError = 0

#------------------------------------------------------------------
#-- Definición del Frame Principal
#------------------------------------------------------------------
mainFrame = Frame( window )
mainFrame.pack()

#------------------------------------------------------------------
#-- Funciones del Menu principal
#------------------------------------------------------------------
# -- Deshabilitando los botones
def doDisableButton():
    buttonSkip.config(state=DISABLED)
    buttonSkipAll.config(state=DISABLED)
    buttonAdd.config(state=DISABLED)
    buttonChange.config(state=DISABLED)
    buttonChangeAll.config(state=DISABLED)

# -- Deshabilitando los botones
def doNormalButton():
    buttonSkip.config(state=NORMAL)
    buttonSkipAll.config(state=NORMAL)
    buttonAdd.config(state=NORMAL)
    buttonChange.config(state=NORMAL)
    buttonChangeAll.config(state=NORMAL)

# -- Siguiente palabra a corregir
def doNextWord():
    global pivotWordsError, totalWordsError, listWordsError, listWordsCorrect
    if pivotWordsError < totalWordsError:
        wordError.set( listWordsError[ pivotWordsError ] )
        listWordsCorrect = getWordsCorrect( wordError.get() )
        setWordsCorrect()
        pivotWordsError += 1
    else:
        wordError.set("")
        doDisableButton()
        messagebox.showinfo("Corrector Ortográfico", "Se finalizó el corrector")

# -- Inicia una nueva corrección
def doNewCorrection():
    global pivotWordsError, totalWordsError
    textTextToCorrect.delete( 1.0, END )
    textTextToCorrect.insert(INSERT, "Coloque aqui el texto a corregir, luego haz click en 'Iniciar Corrector' del menu 'Corrector' ")
    textTextMessage.delete(1.0, END)
    textToCorrect.set( "" )
    wordError.set("")
    listWordsCorrect.clear()
    listboxWord.delete( 0, END )
    listWordsError.clear()
    totalWordsError = 0
    pivotWordsError = 0
    # --Botones
    doDisableButton()

# -- Abrir un archivo txt para corregir
def doOpenFile():
    filename = filedialog.askopenfilename( title = "Abrir archivo a corregir",
                                           filetypes = ( ("Archivo de texto", "*.txt" ), ("Todos los archivos", "*.*" ) ) )
    if filename:
        doNewCorrection()
        fileText = open( filename, "r" )
        textToCorrect.set( fileText.read() )
        textTextToCorrect.insert( INSERT, textToCorrect.get() )
        fileText.close()
# -- Graba el archivo corregido
def doSaveFile():
    filename = filedialog.asksaveasfilename( title = "Guardar archivo corregido",
                                           filetypes = ( ("Archivo de texto", "*.txt" ), ("Todos los archivos", "*.*" ) ) )
    if filename:
        fileText = open(filename, "w")
        textCorrected = textTextToCorrect.get( 1.0, END )
        fileText.write( textCorrected )
        fileText.close()

# -- Inicia la corrección
def doStartCorrect():
    global pivotWordsError, totalWordsError, listWordsError
    messagebox.showinfo( "Corrector Ortográfico", "Se iniciará el corrector")
    textTextMessage.delete(1.0, END)
    #listWordsError = getWordsError( textToCorrect.get() )
    listWordsError = getWordsError(textTextToCorrect.get("1.0", END))
    totalWordsError = len(listWordsError)
    pivotWordsError = 0
    doNormalButton()
    doNextWord()


# -- Muestra el Acerca de
def doAbout():
    messagebox.showinfo( "Corrector Ortográfico", "Proyecto Final - Corrector Ortográfico" + getIntegrantes() )
# -- Para salir del sistema
def doExit():
   resp = messagebox.askyesno( "Salir", "¿Deseas salir del Sistema?" )
   if resp == True:
       window.destroy()

#------------------------------------------------------------------
#-- Definición del Menu Principal
#------------------------------------------------------------------
mainMenu = Menu( window )

fileMenu = Menu( mainMenu, tearoff = False )
fileMenu.add_command( label = "Nueva Corrección", command = doNewCorrection )
fileMenu.add_command( label = "Abrir Archivo", command = doOpenFile )
fileMenu.add_command( label = "Guardar Archivo", command = doSaveFile )
fileMenu.add_separator()
fileMenu.add_command( label = "Salir", command = doExit )

correctMenu = Menu( mainMenu, tearoff = False )
correctMenu.add_command( label = "Iniciar Corrector", command = doStartCorrect )

helpMenu = Menu( mainMenu, tearoff = False )
helpMenu.add_command( label = "Manual de usuario" )
helpMenu.add_separator()
helpMenu.add_command( label = "Acerca del Corrector", command = doAbout )

mainMenu.add_cascade( label = "Archivo", menu = fileMenu )
mainMenu.add_cascade( label = "Corrector", menu = correctMenu )
mainMenu.add_cascade( label = "Ayuda", menu = helpMenu )


#------------------------------------------------------------------
#-- Frame Menu
#menuFrame = Frame( mainFrame, width="100", height="30" )
#menuFrame.pack( side = TOP, fill="x" )
#------------------------------------------------------------------

#------------------------------------------------------------------
#-- Definición del Frame del texto a corregir
#------------------------------------------------------------------
textFrame = Frame( mainFrame, width = "400", height = "400" )
textFrame.pack( side = "left", fill = "y" )

labelTitle = Label( textFrame, text = "Texto a corregir: ")
labelTitle.config( fg = "blue" )
labelTitle.grid( row = 0, column = 0, sticky = "w", pady = 4, padx = 4 )

textTextToCorrect = Text( textFrame, width = "50", height = "10" )
textTextToCorrect.insert( INSERT, textToCorrect.get() )
textTextToCorrect.tag_add("error", "1.8", "1.11")

textTextToCorrect.tag_config( "error", background = "white", foreground = "red", underline = True )
textTextToCorrect.grid( row = 1, column = 0, sticky = "w", pady = 4, padx = 4 )

scrollText = Scrollbar( textFrame, command = textTextToCorrect.yview )
scrollText.grid( row = 1, column = 1, sticky = "nswe" )
textTextToCorrect.config( yscrollcommand = scrollText.set )

textTextMessage = Text( textFrame, width = "50", height = "10" )
textTextMessage.insert( INSERT, "Cambiado \n Saludos" )
textTextMessage.grid( row = 2, column = 0, sticky = "w", pady = 4, padx = 4 )
scrollMessage = Scrollbar( textFrame, command =textTextMessage.yview )
scrollMessage.grid( row = 2, column = 1, sticky = "nswe" )
textTextMessage.config( yscrollcommand = scrollMessage.set )

#------------------------------------------------------------------
#-- Definición de las Funciones de los botones del Frame opciones
#------------------------------------------------------------------
def getWord( tupla ):
    if not len( tupla ):
        return None
    return listWordsCorrect[ int( tupla[ 0 ] ) ]

def setWordsCorrect(  ):
    listboxWord.delete(0, END)
    for item in listWordsCorrect:
        listboxWord.insert(END, item)

def doSkip():
    omitir( wordError.get() )
    messageVar.set("Omitio la corrección de la palabra")
    textTextMessage.insert(INSERT, "\nOmitir: " + wordError.get())
    doNextWord()

def doSkipAll():
    global pivotWordsError, totalWordsError, listWordsError
    listWordsError = omitirTodo( wordError.get(), listWordsError )
    totalWordsError = len(listWordsError)
    pivotWordsError -= 1
    messageVar.set("Omitio la corrección de todas las palabras")
    textTextMessage.insert(INSERT, "\nOmitir todo: " + wordError.get())
    doNextWord()


def doAdd():
    agregar(wordError.get())
    messageVar.set( "Agregando la palabra " +wordError.get() + " al corrector" )
    textTextMessage.insert(INSERT, "\nAgregar: " + wordError.get())
    doNextWord()

def doChange():
    word = getWord( listboxWord.curselection() )
    if word != None:
        messageVar.set( "Corrigiendo la palabra " + wordError.get() + " por " + word )
        textToCorrect.set( cambiar( wordError.get(), word, textToCorrect.get() ) )
        textTextToCorrect.delete(1.0, END)
        textTextToCorrect.insert(INSERT, textToCorrect.get())
        textTextMessage.insert(INSERT, "\nCambiar: " + wordError.get() + ", por: " + word )
        doNextWord()
    else:
        messagebox.showinfo("Corrector Ortográfico", "Seleccione la Palabra a cambiar")

def doChangeAll():
    word = getWord(listboxWord.curselection())
    if word != None:
        messageVar.set("Corrigiendo todas las palabras " + wordError.get() + " por " + word)
        textToCorrect.set(cambiarTodo(wordError.get(), word, textToCorrect.get()))
        textTextToCorrect.delete(1.0, END)
        textTextToCorrect.insert(INSERT, textToCorrect.get())
        textTextMessage.insert(INSERT, "\nCambiar todo: " + wordError.get() + ", por: " + word)
        doNextWord()
    else:
        messagebox.showinfo("Corrector Ortográfico", "Seleccione la Palabra a cambiar")

#------------------------------------------------------------------
#-- Definición del Frame de las opciones/botones
#------------------------------------------------------------------
optionsFrame = Frame( mainFrame, width="300", height="400" )
optionsFrame.pack( side="right", fill="y" )

labelWord = Label( optionsFrame, textvariable = wordError, font="Arial 14" )
labelWord.config( fg = "blue" )
labelWord.grid( row = 0, column = 0, columnspan = 3, sticky = "w", pady = 4, padx = 4 )

buttonSkip = Button( optionsFrame, text = "Omitir", state = DISABLED, command = doSkip )
buttonSkip.grid( row = 1, column = 0, pady = 4, padx = 4 )

buttonSkipAll = Button( optionsFrame, text = "Omitir todo", command = doSkipAll )
buttonSkipAll.grid( row = 1, column = 1, pady = 4, padx = 4 )

buttonAdd = Button( optionsFrame, text = "Agregar", command = doAdd )
buttonAdd.grid( row = 1, column = 2, pady = 4, padx = 4 )

listboxWord = Listbox( optionsFrame )

listboxWord.grid( row = 2, column = 0, columnspan = 3, sticky = "w", pady = 4, padx = 4 )

buttonChange = Button( optionsFrame, text = "Cambiar", command = doChange )
buttonChange.grid( row = 3, column = 0, pady = 4, padx = 4 )

buttonChangeAll = Button( optionsFrame, text = "Cambiar todo", command = doChangeAll )
buttonChangeAll.grid( row = 3, column = 1, pady = 4, padx = 4 )


#------------------------------------------------------------------
#-- Definición del Frame de mensajes
#------------------------------------------------------------------
messageFrame = Frame( window )
messageFrame.pack( side = BOTTOM, fill="x" )
messageFrame.config( relief = "groove", bd = 1 )

message = Label( messageFrame, textvariable = messageVar )
message.pack( side = LEFT, pady = 4, padx = 4 )

#------------------------------------------------------------------
doNewCorrection()
window.config( menu = mainMenu )
window.mainloop();

#------------------------------------------------------------------


#------------------------------------------------------------------
#--
#------------------------------------------------------------------