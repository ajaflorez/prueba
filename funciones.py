# Coloque la lista de los integrantes del grupo
def getIntegrantes():
    return """
    - Jose
    - Maria
    - Jesus
    """

#------------------------------------------------------------------
#-- getWordsError: Funcion que devuelve la lista de las palabra  mal escritas
#-- Parametros de entrada:
#--     text: Texto a analizar
#-- Retorno:
#--     Lista de palabras mal escritas
#------------------------------------------------------------------
def getWordsError( texto ):
    print(texto)
    # Coloque a continuación su código

    # codigo de ejemplo
    lista = ["gat", "bubuja", "bogado", "cascanueze", "cantor"]
    return lista
#
#------------------------------------------------------------------
#-- getWordsCorrect: Funcion que devuelve la lista de las posibles palabras correctas.
#-- Parametros de entrada:
#--     palabra:  palabra mal escrita
#-- Retorno:
#--     lista de las posibles palabras correctas
#------------------------------------------------------------------
def getWordsCorrect( palabra ):
    # Coloque a continuación su código

    # codigo de ejemplo
    lista = ["gata", "gatito", "gato", "gatillo"]
    lista.append(palabra)
    return lista

#------------------------------------------------------------------
#-- omitir: Funcion que Omite la corrección de la palabra mal escrita actual
#-- Parametros de entrada:
#--     palabra: palabra omitida
#------------------------------------------------------------------
def omitir( palabra ):
    # Coloque a continuación su código

    # codigo de ejemplo
    print( "Omitiendo la palabra: ", palabra )

#------------------------------------------------------------------
#-- omitirTodo: Funcion que Omite la corrección de todas las palabras mal escritas
#--     iguales a la "palabra mal escrita actual"
#--     * Quita la palabra de la lista de palabras a corregir
#-- Parametros de entrada:
#--     palabra: palabra omitida
#--     lista: Lista de palabras mal escritas
#-- Retorno:
#--     Lista nueva de palabras mal escritas sin la palabra omitida
#------------------------------------------------------------------
def omitirTodo( palabra, lista ):
    # Coloque a continuación su código

    # codigo de ejemplo
    listanueva = ["abioneta", "escarravajo"]
    return listanueva

#------------------------------------------------------------------
#-- agregar: Funcion que Agrega la "palabra mal escrita" al diccionario para que se
#       reconozca como correcta
#-- Parametros de entrada:
#--     palabra: palabra a agregar
#------------------------------------------------------------------
def agregar( palabra ):
    # Coloque a continuación su código

    # codigo de ejemplo
    print( "Agregando la palabra: ", palabra )

#------------------------------------------------------------------
#-- cambiar: Función que Cambia la palabra "mal escrita" por la "palabra correcta"
#--     en el texto
#-- Parametros de entrada:
#--     palabraMal: palabra mal escrita
#--     palabraBien: palabra bien escrita o correctamente escrita
#--     texto: texto a cambiar
#-- Retorno:
#--     Texto con la palabra mal escrita cambiada
#------------------------------------------------------------------
def cambiar( palabraMal, palabraBien, texto ):
    # Coloque a continuación su código

    # codigo de ejemplo
    print( "Cambiando la palabra: ", palabraMal, " por: ", palabraBien )
    return "El texto está correctamente escrito"

#------------------------------------------------------------------
#-- cambiarTodo: Función que Cambia todas las "palabras mal escritas" iguales a la palabra "mal escrita actual"
#--     por la palabra correcta en el texto
#-- Parametros de entrada:
#--     palabraMal: palabra mal escrita
#--     palabraBien: palabra bien escrita o correctamente escrita
#--     texto: texto a cambiar
#-- Retorno:
#--     Texto con la palabra mal escrita cambiada
#------------------------------------------------------------------
def cambiarTodo( palabraMal, palabraBien, texto ):
    # Coloque a continuación su código

    # codigo de ejemplo
    print( "Cambiando todas las palabras: ", palabraMal, " por: ", palabraBien )
    return "El texto está totalmente corregido"


